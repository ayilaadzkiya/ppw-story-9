from django.contrib import admin
from django.urls import path
from mywebsite import views

app_name = 'mywebsite'

urlpatterns = [
    path('', views.Halaman_LogIn), #tampilin halaman awal
    path('welcome/', views.Welcome_LogIn),
    path('keluar/', views.Keluar_LogOut),


]