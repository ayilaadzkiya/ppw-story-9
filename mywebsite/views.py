from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout


# Create your views here.

def Halaman_LogIn(request):
    if request.user.is_authenticated:
        return redirect("welcome/")
    else:
        if request.method == "GET": 
            return render(request, "Index9.html")
        elif request.method == "POST":
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                request.session['nama_depan'] = request.user.first_name
                request.session['nama_belakang'] = request.user.last_name
                return redirect("welcome/")
            else:
                return redirect("/")

def Welcome_LogIn(request):
    if not request.user.is_authenticated:
        return redirect('/')
    else:
        return render(request, "Welcome.html")

def Keluar_LogOut(request):
    logout(request)
    return redirect("/")